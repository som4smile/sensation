//
//  APIManager.swift
//  Sensation
//
//  Created by SOM on 18/08/19.
//  Copyright © 2019 Somnath Rasal. All rights reserved.
//

import SwiftyJSON
import Alamofire

class APIManager {
    static let sharedInstance = APIManager()
    
    static let baseURL = "http://saurabhzodgekar.com/lmsapi/Auth/"
    
    func callLoginAPI(userName: String, password: String, success: @escaping (_ JSON: Any) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        let url = "\(type(of: self).baseURL)login"
        let parameters: [String: Any] = ["username": userName, "password": password]
        self.callAPI(url: url, parameters: parameters, success: success, failure: failure)
    }
    
    func callForgotPasswordAPI(email: String, success: @escaping (_ JSON: Any) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        let url = "\(type(of: self).baseURL)forgotpassword"
        let parameters: [String: Any] = ["email": email]
        self.callAPI(url: url, parameters: parameters, success: success, failure: failure)
    }
    
    func callVerifyOTPAPI(email: String, otp: Int, success: @escaping (_ JSON: Any) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        let url = "\(type(of: self).baseURL)verifyotp"
        let parameters: [String: Any] = ["email": email, "otp": otp]
        self.callAPI(url: url, parameters: parameters, success: success, failure: failure)
    }

    func callUpdatePasswordAPI(email: String, password: String, success: @escaping (_ JSON: Any) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        let url = "\(type(of: self).baseURL)update_password"
        let parameters: [String: Any] = ["email": email, "password": password]
        self.callAPI(url: url, parameters: parameters, success: success, failure: failure)
    }

    private func getHeader() -> [String: String] {
        let headers: [String: String] = [
            "Client-Service": "frontend-client",
            "Auth-Key": "simplerestapi",
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Basic Og=="
        ]
        return headers
    }
    func callAPI(url: String, parameters: [String: Any], success: @escaping (_ JSON: Any) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: self.getHeader()).responseJSON { response in
            
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
                if let dict = json as? [String: AnyObject],
                    let status = dict["status"] as? Int {
                    if status == 200 {
                        success(json)
                    } else {
                        let message = dict["message"] as? String ?? ""
                        let error = NSError(domain: "", code: status, userInfo: [NSLocalizedDescriptionKey: message])
                        failure(error)
                    }
                }
            }
            if let error = response.error {
                failure(error)
            }
        }
    }
}
