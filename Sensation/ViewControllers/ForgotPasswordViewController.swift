//
//  ForgotPasswordViewController.swift
//  Sensation
//
//  Created by SOM on 03/08/19.
//  Copyright © 2019 Somnath Rasal. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController, UITextFieldDelegate {
   
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailTextField.text = "rasal.somnath@gmail.com"
    }
    
    @IBAction func forgotPasswordClicked(_ sender: Any) {
        let email = self.emailTextField.text ?? ""
        
        if email.isEmpty {
            self.showAlert(withTitle: "Error!", withMessage: "Please Enter Email")
        } else if !SesnsationHelper.isValidEmail(email) {
            self.showAlert(withTitle: "Error!", withMessage: "Please Enter Valid Email")
        } else {
            self.showSpinner(onView: self.view)
            APIManager.sharedInstance.callForgotPasswordAPI(email: email, success: { json in
                print("Success: \(json)")
                self.removeSpinner()
                let viewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OTPVerificationViewController") as? OTPVerificationViewController
                self.navigationController?.pushViewController(viewController!, animated: true)
            }, failure: { error in
                self.removeSpinner()
                if let err = error {
                    print("\nError: " + err.localizedDescription)
                }
            })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
