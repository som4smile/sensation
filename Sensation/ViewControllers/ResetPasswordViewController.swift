//
//  ResetPasswordViewController.swift
//  Sensation
//
//  Created by SOM on 04/08/19.
//  Copyright © 2019 Somnath Rasal. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.passwordTextField.text = "somnath123321"
        self.repasswordTextField.text = "somnath123321"
    }
    
    @IBAction func resetPasswordClicked(_ sender: Any) {
        let password = self.passwordTextField.text ?? ""
        let repassword = self.repasswordTextField.text ?? ""
        
        if password.isEmpty {
            self.showAlert(withTitle: "Error!", withMessage: "Please enter password")
        } else if repassword.isEmpty {
            self.showAlert(withTitle: "Error!", withMessage: "Please re-enter password")
        } else if password !=  repassword {
            self.showAlert(withTitle: "Error!", withMessage: "Password and Re-password does not match")
        } else {
            self.showSpinner(onView: self.view)
            APIManager.sharedInstance.callUpdatePasswordAPI(email: "rasal.somnath@gmail.com", password: password, success: { json in
                self.removeSpinner()
                print("Success: \(json)")
            }, failure: { error in
                self.removeSpinner()
                if let err = error {
                    print("\nError: " + err.localizedDescription)
                }
            })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
