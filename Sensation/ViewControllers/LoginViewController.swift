//
//  LoginViewController.swift
//  Sensation
//
//  Created by SOM on 03/08/19.
//  Copyright © 2019 Somnath Rasal. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailTextField.text = "rasal.somnath@gmail.com"
        self.passwordTextField.text = "somnath123456"
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        let email = self.emailTextField.text ?? ""
        let password = self.passwordTextField.text ?? ""
        
        if email.isEmpty {
            self.showAlert(withTitle: "Error!", withMessage: "Please Enter Email")
        } else if !SesnsationHelper.isValidEmail(email) {
            self.showAlert(withTitle: "Error!", withMessage: "Please Enter Valid Email")
        } else if self.passwordTextField.text!.isEmpty {
            self.showAlert(withTitle: "Error!", withMessage: "Please Enter Password")
        } else {
            self.showSpinner(onView: self.view)
            APIManager.sharedInstance.callLoginAPI(userName: email, password: password, success: { json in
                print("Success: \(json)")
                self.removeSpinner()
            }, failure: { error in
                self.removeSpinner()
                if let err = error {
                    print("\nError: " + err.localizedDescription)
                }
            })
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
