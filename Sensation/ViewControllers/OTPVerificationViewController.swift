//
//  OTPVerificationViewController.swift
//  Sensation
//
//  Created by SOM on 03/08/19.
//  Copyright © 2019 Somnath Rasal. All rights reserved.
//

import UIKit

class OTPVerificationViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var otpTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func verifyOTPClicked(_ sender: Any) {
        let otpString = self.otpTextField.text ?? ""
        if otpString.isEmpty {
            self.showAlert(withTitle: "Error!", withMessage: "Please Enter OTP")
        } else {
            self.showSpinner(onView: self.view)
            let otp = Int(otpString) ?? 0
            APIManager.sharedInstance.callVerifyOTPAPI(email: "rasal.somnath@gmail.com", otp: otp, success: { json in
                print("Success: \(json)")
                self.removeSpinner()
                let viewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ResetPasswordViewController") as? ResetPasswordViewController
                self.navigationController?.pushViewController(viewController!, animated: true)
            }, failure: { error in
                if let err = error {
                    print("\nError: " + err.localizedDescription)
                }
                self.removeSpinner()
            })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
