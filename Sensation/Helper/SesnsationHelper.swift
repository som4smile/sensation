//
//  SesnsationHelper.swift
//  Sensation
//
//  Created by SOM on 18/08/19.
//  Copyright © 2019 Somnath Rasal. All rights reserved.
//

import Foundation

class SesnsationHelper {
    
    class func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }

    static func isValidString(_ value: String) -> Bool {
        return !value.isEmpty
    }
}
